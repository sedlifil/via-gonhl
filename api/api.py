import datetime

from flask import request, jsonify, Response
from tinydb import TinyDB
from tinydb import where

from api.__init__ import bp


@bp.route('/news', methods=['POST', 'GET'])
def news():
    if request.method == 'POST':
        db = TinyDB('db.json')

        today = datetime.date.today()
        data = request.json
        if "title" in data and "author" in data and "description" in data and "content" in data:
            if "urlToImage" in data:
                id = db.insert({"news": "own", "date": str(today), "title": data["title"], "author": data["author"],
                                "description": data["description"], "content": data["content"],
                                "urlToImage": data["urlToImage"]})
            else:
                id = db.insert({"news": "own", "date": str(today), "title": data["title"], "author": data["author"],
                                "description": data["description"], "content": data["content"]})
            db.update({"link": id}, doc_ids=[id])
        else:
            return Response("{'message': 'You have to specify: title, author, description and content, "
                            "urlToImage is optional.'}", status=406,
                            mimetype='application/json')

        return jsonify(message="success")
    elif request.method == 'GET':
        db = TinyDB('db.json')
        data = db.search(where('news') == "own")
        return jsonify(data)
