import os
from datetime import datetime

import pytz
from flask import Flask
from flask import render_template

app = Flask(__name__)


@app.template_filter('datetimefilter')
def datetimefilter(value):
    tz = pytz.timezone('Europe/Paris') # timezone you want to convert to from UTC
    utc = pytz.timezone('UTC')
    unaware_est = datetime.strptime(value, "%Y-%m-%dT%H:%M:%SZ")
    value = utc.localize(unaware_est, is_dst=None).astimezone(pytz.utc)
    local_dt = value.astimezone(tz)
    new_format = "%H:%M"
    return local_dt.strftime(new_format)


def create_app():
    from errors.__init__ import bp as errors_bp
    app.register_blueprint(errors_bp)

    from rest.__init__ import bp as rest_bp
    app.register_blueprint(rest_bp, url_prefix='/')

    from api.__init__ import bp as api_bp
    app.register_blueprint(api_bp, url_prefix='/api')
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
    return app



def index():
    return render_template('home.html', title='Home')


app = create_app()


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
