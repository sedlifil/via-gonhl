import datetime
import json

import dateutil
import requests
from dateutil.relativedelta import *
from flask import render_template
from tinydb import TinyDB, Query
from tinydb import where

from rest.__init__ import bp


@bp.route('/')
@bp.route('/index')
def home():
    return render_template('home.html', title='Home')


@bp.route('/teams/<int:id>', methods=['GET'])
def teams_by_id(id):
    db = TinyDB('db.json')

    url = "https://statsapi.web.nhl.com/api/v1/teams/" + str(id)
    response = requests.get(url)
    content = json.loads(response.content)
    team = content["teams"][0]
    today = datetime.date.today()
    yesterday = today + dateutil.relativedelta.relativedelta(days=-1)
    next_month = today + dateutil.relativedelta.relativedelta(months=1)
    last_month = today + dateutil.relativedelta.relativedelta(months=-1)

    url = "https://statsapi.web.nhl.com/api/v1/teams/" + str(id) + "/roster"
    response = requests.get(url)
    content = json.loads(response.content)
    roster = content["roster"]
    centers = []
    left_wings = []
    right_wings = []
    defenders = []
    goalies = []
    # statistics for individula players
    team_date_flag = db.search(where('team') == id)
    Team = Query()
    for rost in roster:
        person_record = db.search(where('person') == rost["person"]["id"])
        if team_date_flag != [] and str(today) == team_date_flag[0]["date"] and person_record != []:
            content = person_record[0]["data"]

        else:
            url = "https://statsapi.web.nhl.com/api/v1/people/" + str(rost["person"]["id"]) \
                  + "/stats?stats=statsSingleSeason&season=20182019"
            response = requests.get(url)
            content = json.loads(response.content)
            if not person_record:
                db.insert({"person": rost["person"]["id"], "data": content})
            else:
                db.update({"person": rost["person"]["id"], "data": content}, Team.person == rost["person"]["id"])

        if len(content["stats"]) <= 0 or len(content["stats"][0]["splits"]) <= 0:
            print("")
        else:
            rost.update(content["stats"][0]["splits"][0]["stat"])

        if rost["position"]["code"] == "C":
            centers.append(rost)
        elif rost["position"]["code"] == "L":
            left_wings.append(rost)
        elif rost["position"]["code"] == "R":
            right_wings.append(rost)
        elif rost["position"]["code"] == "D":
            defenders.append(rost)
        elif rost["position"]["code"] == "G":
            goalies.append(rost)

    # change date in team id in db to -> today or insert one
    Team = Query()
    if not team_date_flag:
        db.insert({"team": id, "date": str(today)})
    else:
        db.update({"date": str(today)}, Team.team == id)

    # latest game
    url = "https://statsapi.web.nhl.com/api/v1/schedule?teamId=" + str(id) + "&startDate=" + str(
        last_month) + "&endDate=" + str(yesterday)
    response = requests.get(url)
    content = json.loads(response.content)
    last_game = {}
    if content["totalGames"] != 0:
        if len(content["dates"]) != 0:
            last = content["dates"][-1]
            if len(last["games"]) != 0:
                last = last["games"][-1]
                url = "https://statsapi.web.nhl.com/api/v1/game/" + str(last["gamePk"]) + "/feed/live"
                response = requests.get(url)
                content = json.loads(response.content)
                last_game = content

    # next games in 30 days
    url = "https://statsapi.web.nhl.com/api/v1/schedule?teamId=" + str(id) + "&startDate=" + str(
        today) + "&endDate=" + str(next_month)
    response = requests.get(url)
    content = json.loads(response.content)
    return render_template('team.html', team=team, title=team["name"], games=content, centers=centers,
                           left_wings=left_wings, right_wings=right_wings, defenders=defenders, goalies=goalies,
                           last_game=last_game)


@bp.route('/teams', methods=['GET'])
def teams():
    url = "https://statsapi.web.nhl.com/api/v1/teams"
    response = requests.get(url)
    content = json.loads(response.content)
    teams_dict = content["teams"]
    return render_template('teams.html', teams=teams_dict, title="Teams")


@bp.route('/stats', methods=['GET'])
def statistics():
    url = "https://statsapi.web.nhl.com/api/v1/standings"
    response = requests.get(url)
    content = json.loads(response.content)
    metropolitan = {}
    atlantic = {}
    central = {}
    pacific = {}

    for stat in content["records"]:
        if stat["division"]["name"] == "Metropolitan":
            metropolitan = stat
        elif stat["division"]["name"] == "Atlantic":
            atlantic = stat
        elif stat["division"]["name"] == "Central":
            central = stat
        elif stat["division"]["name"] == "Pacific":
            pacific = stat

    return render_template('stats.html', title="Statistics", metropolitan=metropolitan, atlantic=atlantic,
                           central=central, pacific=pacific)



@bp.route('/news', methods=['GET'])
def headlines():
    url = "https://newsapi.org/v2/everything?sources=nhl-news&apiKey=57e1f499b2b345898b6372cbef2e66a7"
    response = requests.get(url)
    content = json.loads(response.content)
    news = content["articles"]
    db = TinyDB('db.json')
    news_from_db = db.search(where('news') == "own")
    return render_template('news.html', news=news, news_from_db=news_from_db, title="News")


@bp.route('/news/<int:id>', methods=['GET'])
def news_one(id):
    db = TinyDB('db.json')
    news_from_db = db.search(where('link') == id)
    return render_template('news_one.html', news_from_db=news_from_db[0], title="News")


@bp.route('/api', methods=['GET'])
def api():
    return render_template('api.html', title="goNHL API")
